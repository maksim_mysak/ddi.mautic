<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

require_once  $_SERVER["DOCUMENT_ROOT"] .'/bitrix/modules/ddi.mautic/lib/vendor/autoload.php';
IncludeModuleLangFile(__FILE__);
/****************************************************************************/
/***********  MAIN PAGE  ****************************************************/
/****************************************************************************/

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

if($_REQUEST['action_test'] =='test') {
    if (CModule::IncludeModule("ddi.mautic")){
        $res_test =  \MauticBitrix::TestApi();


        if(strlen($res_test['error']) || $res_test['success']['error']['code']==500) {

            if (strlen($res_test['error_description'])==0) {
                $res_test['error_description'] = $res_test['success']['error']['message'];
            }
            CAdminMessage::ShowMessage(array(
                "MESSAGE"=>"Send Test Payload" . $res_test['error'],
                "DETAILS"=> $res_test['error_description'] ."<br/>".
                "<a href='/bitrix/admin/settings.php?mid=ddi.mautic&lang=ru'>Go to settings module </a>",
                "HTML"=>true,
                "TYPE"=>"ERROR"
            ));
        } else {
            CAdminMessage::ShowMessage(array(
                "MESSAGE"=>"Send Test Payload",
                "DETAILS"=> '',
                "HTML"=>true,
                "TYPE"=>"OK"
            ));
            ?>
            <textarea name="" id="" cols="30" rows="10"><?=json_encode($res_test['success'], true);?></textarea>
            <?
        }
    }
}?>
<form action="/bitrix/admin/ddi_mautic_settings.php" method="post">
    <input type="hidden" value="test" name="action_test">
    <input class="adm-btn adm-btn-save" type="submit" value="Send Test Payload">
</form>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
?>
