<?
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

AddEventHandler("main", "OnBuildGlobalMenu", "global_menu_ddi_mautic");

function global_menu_ddi_mautic(&$aGlobalMenu, &$aModuleMenu){
	$aModuleMenu[] = array(
		"parent_menu" => "global_menu_settings",
		"icon"        => "default_menu_icon",
		"page_icon"   => "default_page_icon",
		"text"        => Loc::getMessage("DDI_MAUTIC_ADMIN_MENU_SETTINGS_TEXT"),
		"title"       => Loc::getMessage("DDI_MAUTIC_ADMIN_MENU_SETTINGS_TITLE"),
		"url"         => "ddi_mautic_settings.php",
	);

}
?>