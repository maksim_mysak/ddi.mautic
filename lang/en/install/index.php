<?
$MESS["DDI_MAUTIC_MODULE_NAME"] = "Mautic api integration";
$MESS["DDI_MAUTIC_MODULE_DESC"] = "Mautic api integration";
$MESS["DDI_MAUTIC_PARTNER_NAME"] = "ddi";
$MESS["DDI_MAUTIC_PARTNER_URI"] = "http://ddi-dev.com/";

$MESS["DDI_MAUTIC_INSTALL"] = "Installing the module\"Mautic api integration\"";
$MESS["DDI_MAUTIC_UNINSTALL"] = "Uninstalling the module \"Mautic api integration\"";

$MESS["DDI_MAUTIC_INSTALL_ERROR_VERSION"] = "The main module version is lower than 14. D7 technology is not supported. Please update the system.";

$MESS["DDI_MAUTIC_IBLOCK_TYPE_NAME_EN"] = "ddi.mautic";
$MESS["DDI_MAUTIC_IBLOCK_TYPE_NAME_RU"] = "Mautic api integration";
$MESS["DDI_MAUTIC_IBLOCK_TYPE_ALREADY_EXISTS"] = "This type of information block already exists";
$MESS["DDI_MAUTIC_IBLOCK_ALREADY_EXISTS"] = "Infoblock with this code already exists";
$MESS["DDI_MAUTIC_IBLOCK_TYPE_DELETION_ERROR"] = "Error removing information block type";
?>