<?
require_once  $_SERVER["DOCUMENT_ROOT"] .'/bitrix/modules/ddi.mautic/lib/vendor/autoload.php';

use Mautic\Auth\ApiAuth;
use Mautic\MauticApi;

$mautic_site = COption::GetOptionString("ddi.mautic","mautic_site");
$mautic_user = COption::GetOptionString("ddi.mautic","mautic_user");
$mautic_pass = COption::GetOptionString("ddi.mautic","mautic_pass");

define("AC_MAUTIC_URL", $mautic_site);
define("AC_MAUTIC_USER", $mautic_user);
define("AC_MAUTIC_PASSWORD", $mautic_pass);

class MauticBitrix {

    /**
     * @return bool
     */
    function config() {
        if (
            (strlen(AC_MAUTIC_URL) > 0) &&
            (strlen(AC_MAUTIC_USER) > 0) &&
            (strlen(AC_MAUTIC_PASSWORD) > 0)
        ) {
            return true;
        } else {
            Bitrix\Main\Diag\Debug::writeToFile(
                "Error now found config",
                date("Y/m/d H:i:s"),
                "/bitrix/modules/ddi.mautic/log/error.log");
            return false;
        }
    }

    /**
     * @return mixed
     */
    public function getRequest()
    {
        $rawData = file_get_contents("php://input");
        return json_decode($rawData, true);
    }

    public  function addLog($data, $mess = false){
        if (COption::GetOptionString("ddi.mautic","mautic_log") =='Y'){
            Bitrix\Main\Diag\Debug::writeToFile(
                $data,
                $mess. ' '.date("Y/m/d H:i:s"),
                "/bitrix/modules/ddi.mautic/log/log.log"
            );
        }
    }

    /**
     * @return bool|\Mautic\Auth\AuthInterface
     */
    public function Auth(){
        $settings = array(
            'userName'   => AC_MAUTIC_USER,
            'password'   => AC_MAUTIC_PASSWORD
        );
        if (self::config()) {
            $initAuth = new ApiAuth();
            $auth = $initAuth->newAuth($settings, 'BasicAuth');
            return $auth;
        } else {
            return false;
        }
    }

    /**
     * @return array
     */
    public function TestApi()
    {
        $result = [];

        if (self::config()){
            $auth = self::Auth();
            $api = new MauticApi();
            $contactApi = $api->newApi('contacts', $auth, AC_MAUTIC_URL . "/api");
            $contacts = $contactApi->getList(false, 0, 1);
            if (strlen($contacts['error'])){
                $result['error'] = $contacts['error'];
                $result['error_description'] = $contacts['error_description'];
            } else {
                $result['success'] = $contacts;
            }
        } else {
            $result['error'] = 'Enter your login password and URL Mautic';

        }
        return $result;
    }

    /**
     * @param $id
     * @return array|mixed
     */
    public function getLeadFromMauticById($id)
    {
        $auth = self::Auth();
        $api = new MauticApi();
        $contactApi = $api->newApi('contacts', $auth, AC_MAUTIC_URL . "/api");
        $contact = $contactApi->get($id);
        if (!empty($contact['errors'])){
            Bitrix\Main\Diag\Debug::writeToFile(
                $contact,
                date("Y/m/d H:i:s"),
                "/bitrix/modules/ddi.mautic/log/error.log");
            return false;
        }
        return $contact;
    }

    /**
     * @param $fields
     * @return array
     */
    public  function CheckListFiels($fields){
        $cheach_fields = array();
        $cheach_fields_real = array();
        foreach ($fields['fields'] as $k => $v) {
            if ( count($v) >0 ) {
                foreach ($v as $k_f => $t) {
                    if ($k == 'core' && $k_f== 'title') continue;
                    if ($k == 'core' && $k_f== 'firstname') continue;
                    if ($k == 'core' && $k_f== 'lastname') continue;
                    if ($k == 'core' && $k_f== 'company') continue;
                    if ($k == 'core' && $k_f== 'position') continue;
                    if ($k == 'core' && $k_f== 'address1') continue;
                    if ($k == 'all') continue;
                    $code = self::NameField($k, $k_f);
                    $cheach_fields_real[$code] = $k_f;
                    $cheach_fields[$code] = $t['label'];
                }
            }
        }
        $filds_in_site = array();
        $rsData = CUserTypeEntity::GetList( array(), array('ENTITY_ID'=>'CRM_LEAD') );
        while($arRes = $rsData->Fetch())
        {
            $filds_in_site[$arRes["FIELD_NAME"]] = $arRes["FIELD_NAME"];
        }

        foreach ($cheach_fields as $code => $name) {
            if (in_array($code, $filds_in_site) === false) {
                $arFields = Array(
                    "ENTITY_ID" => "CRM_LEAD",
                    "FIELD_NAME" => $code,
                    "USER_TYPE_ID" => "string",
                    "XML_ID" => $cheach_fields_real[$code],
                    "EDIT_FORM_LABEL" => Array("en"=>$name)
                );
                $obUserField  = new CUserTypeEntity;
                $obUserField->Add($arFields);
            }
        }

        return $cheach_fields;
    }

    /**
     * @param $k
     * @param $k_f
     * @return string
     */
    public function NameField($k, $k_f){
        $code = 'UF_MT_'.strtoupper($k).'_'.strtoupper($k_f);

        if (strlen($code)>20) {
            return mb_strimwidth($code, 0, 20, "");
        }else{
            return $code;
        }
    }

    /**
     * @param $fields
     * @return array
     */
    public function ConvertFieldsToBitrix($fields)
    {
        $filds = self::CheckListFiels($fields);

        $arNewFields = array(
            'TITLE' => $fields['fields']['core']['title']['value'],
            'NAME' => $fields['fields']['core']['firstname']['value'],
            'LAST_NAME' => $fields['fields']['core']['lastname']['value'],
            'COMPANY_TITLE' => $fields['fields']['core']['company']['value'],
            'POST' => $fields['fields']['core']['position']['value'],
            'ADDRESS' =>$fields['fields']['core']['address1']['value'],
        );

        foreach ($fields['fields'] as $k => $v) {
            if ( count($v) >0 ) {
                foreach ($v as $k_f => $t) {
                    if ($k == 'core' && $k_f== 'title') continue;
                    if ($k == 'core' && $k_f== 'firstname') continue;
                    if ($k == 'core' && $k_f== 'lastname') continue;
                    if ($k == 'core' && $k_f== 'company') continue;
                    if ($k == 'core' && $k_f== 'position') continue;
                    if ($k == 'core' && $k_f== 'address1') continue;
                    if ($k == 'all') continue;

                    $code = self::NameField($k, $k_f);
                    $arNewFields[$code] = $t['value'];
                }
            }
        }

        return $arNewFields;
    }

    /**
     * @param $arfields
     * @param $bitrix_id
     */
    public function UpdateLeadBitrixFromMautic($arfields, $bitrix_id)
    {
        if (\Bitrix\Main\Loader::includeModule('crm'))
        {
            COption::SetOptionString("main","mautic_stop_event","Y");

            $entity = new CCrmLead(false);
            $fields = self::ConvertFieldsToBitrix($arfields['contact']);
            $id = $entity->update($bitrix_id, $fields);
            self::addLog($id, 'UpdateLeadBitrixFromMautic');
            COption::SetOptionString("main","mautic_stop_event","N");
        }
    }

    /**
     * @param $arfields
     * @param $mautic_id
     */
    public function AddLeadBitrixFromMautic($arfields, $mautic_id)
    {
        //https://dev.1c-bitrix.ru/support/forum/forum48/topic35330/
        if (\Bitrix\Main\Loader::includeModule('crm'))
        {
            COption::SetOptionString("main","mautic_stop_event","Y");

            $entity = new CCrmLead(false);
            $fields = self::ConvertFieldsToBitrix($arfields['contact']);
            $fields['UF_MAUTIC_ID'] = $mautic_id;
            $id =  $entity->add($fields);
            self::addLog($id, 'AddLeadBitrixFromMautic');
            COption::SetOptionString("main","mautic_stop_event","N");
        }
    }

    /**
     * @param $bitrix_id
     */
    public function UpdateLeadMauticFromBitrix($bitrix_id)
    {
        Bitrix\Main\Loader::includeModule('crm');
        Bitrix\Main\Diag\Debug::writeToFile( 'not',"","/eve.log");
        $arFilter = array(
            'ENTITY_ID'  => 'LEAD',
            'CHECK_PERMISSIONS' => 'N',
            '=ID' =>$bitrix_id
        );

        $lead_bitrix = CCrmLead::GetList(array(), $arFilter,
            array('TITLE', 'NAME', 'LAST_NAME', 'COMPANY_TITLE', 'POST', 'ADDRESS', 'UF_*') );
        if ($lead = $lead_bitrix->GetNext()) {
            $id = $lead['UF_MAUTIC_ID'];
            if ($id > 0) {

                $auth = self::Auth();
                $api = new MauticApi();

                $contactApi = $api->newApi('contacts', $auth, AC_MAUTIC_URL . "/api");
                $contact = $contactApi->get($id);
                unset($contact['contact']['fields']['all']['id']);
                $data = $contact['contact']['fields']['all'];

                $data['title'] = $lead['TITLE'];
                $data['firstname'] = $lead['NAME'];
                $data['lastname'] = $lead['LAST_NAME'];
                $data['company'] = $lead['COMPANY_TITLE'];
                $data['position'] = $lead['POST'];
                $data['address1'] = $lead['ADDRESS'];

                $rsData = CUserTypeEntity::GetList( array(), array('ENTITY_ID'=>'CRM_LEAD', '!XML_ID'=>false) );
                while($arRes = $rsData->Fetch())
                {
                    if(strlen($arRes["XML_ID"])>0)
                        $data[$arRes["XML_ID"]] = $lead[$arRes["FIELD_NAME"]];
                }
                $contact_update = $contactApi->edit($id, $data);
                self::addLog($contact_update, 'UpdateLeadMauticFromBitrix');
                COption::SetOptionString("main","bitrix_stop_event","Y");
            }
        }
    }

    /**
     * @param $bitrix_id
     * @param $arFields
     */
    public function AddLeadMauticFromBitrix($bitrix_id, $arFields)
    {
        $auth = self::Auth();
        $api = new MauticApi();
        $contactApi = $api->newApi('contacts', $auth, AC_MAUTIC_URL . "/api");

        $data = array(
            'title'    => $arFields['TITLE'],
            'firstname'=> $arFields['NAME'],
            'lastname' => $arFields['LAST_NAME'],
            'company'  => $arFields['COMPANY_TITLE'],
            'address1' => $arFields['ADDRESS'],
        );
        $rsData = CUserTypeEntity::GetList( array(), array('ENTITY_ID'=>'CRM_LEAD') );
        while($arRes = $rsData->Fetch())
        {
            if (strlen($arRes["XML_ID"]) > 0) {
                $data[$arRes["XML_ID"]] = $arFields[$arRes["FIELD_NAME"]];
            }
        }

        $contact = $contactApi->create($data);
        if ($contact['contact']['id'] > 0) {
            global $USER_FIELD_MANAGER;
            $USER_FIELD_MANAGER->Update( 'CRM_LEAD', $bitrix_id, array(
                'UF_MAUTIC_ID'  => $contact['contact']['id']
            ) );
        }

    }
    /**
     * @param $id
     * @return bool
     */
    public function FindMauticLeadOnCrm($id)
    {
        \Bitrix\Main\Loader::includeModule('crm');
        $arFilter = array(
            'ENTITY_ID'  => 'LEAD',
            'UF_MAUTIC_ID' => intval($id),
            'CHECK_PERMISSIONS' => 'N'
        );
        $ResLeadId = CCrmLead::GetList(array(), $arFilter, array('ID','LEAD_ID') );
        if ($lead_id = $ResLeadId->fetch()) {
            return $lead_id['ID'];
        }

        return false;
    }

    /**
     * get webhook data from mautic site
     */
    public function webhookMautic()
    {
        if ( COption::GetOptionString("main","bitrix_stop_event")=='Y') {
            COption::SetOptionString("main","bitrix_stop_event","N");
            return;
        }

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $request_date = self::getRequest();
            $update_lead_id = intval($request_date['mautic.lead_post_save_update'][0]['lead']['id']);
            if ($update_lead_id > 0) {
                $lead  = self::getLeadFromMauticById($update_lead_id);
                if ($lead['contact']['id'] > 0) {

                    $bitrix_lead_id =  self::FindMauticLeadOnCrm($lead['contact']['id']);
                    //find Mautic id in crm bitrix
                    if ($bitrix_lead_id) {
                        // update field bitrix from
                        self::UpdateLeadBitrixFromMautic($lead, $bitrix_lead_id);
                    } else {
                        // add lead from mautic to bitrix
                        self::AddLeadBitrixFromMautic($lead, $lead['contact']['id']);
                    }
                }
            }

            $lead_post_save_new = intval($request_date['mautic.lead_post_save_new'][0]['lead']['id']);
            if ($lead_post_save_new > 0) {
                $lead  = self::getLeadFromMauticById($lead_post_save_new);
                if ($lead['contact']['id'] > 0) {
                    // add lead from mautic to bitrix
                    self::AddLeadBitrixFromMautic($lead, $lead['contact']['id']);
                }
            }
        }
    }
}
?>