<?
use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Application;

Loc::loadMessages(__FILE__);

Class ddi_mautic extends CModule{
	var	$MODULE_ID = 'ddi.mautic';
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;

	function __construct(){
		$arModuleVersion = array();
		include(__DIR__."/version.php");
		$this->MODULE_VERSION = $arModuleVersion["VERSION"];
		$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		$this->MODULE_NAME = Loc::getMessage("DDI_MAUTIC_MODULE_NAME");
		$this->MODULE_DESCRIPTION = Loc::getMessage("DDI_MAUTIC_MODULE_DESC");

		$this->PARTNER_NAME = getMessage("DDI_MAUTIC_PARTNER_NAME");
		$this->PARTNER_URI = getMessage("DDI_MAUTIC_PARTNER_URI");

		$this->exclusionAdminFiles=array(
			'..',
			'.',
			'menu.php',
			'operation_description.php',
			'task_description.php'
		);
	}

	function InstallDB($arParams = array()){
		$this->createNecessaryIblocks();
        $this->createNecessaryUserFields();
	}

	function UnInstallDB($arParams = array()){
		\Bitrix\Main\Config\Option::delete($this->MODULE_ID);
		$this->deleteNecessaryIblocks();
        $this->deleteNecessaryUserFields();
	}

	function InstallEvents(){
		\Bitrix\Main\EventManager::getInstance()->registerEventHandler("crm", "OnAfterCrmLeadAdd", $this->MODULE_ID, '\Ddi\Mautic\EventHandlers\OnAfterCrmLeadAddHandler', "handler");
		\Bitrix\Main\EventManager::getInstance()->registerEventHandler("crm", "OnAfterCrmLeadUpdate", $this->MODULE_ID, '\Ddi\Mautic\EventHandlers\OnAfterCrmLeadUpdateHandler', "handler");

}

	function UnInstallEvents(){
		\Bitrix\Main\EventManager::getInstance()->unRegisterEventHandler("crm", "OnAfterCrmLeadAdd", $this->MODULE_ID, '\Ddi\Mautic\EventHandlers\OnAfterCrmLeadAddHandler', "handler");
		\Bitrix\Main\EventManager::getInstance()->unRegisterEventHandler("crm", "OnAfterCrmLeadUpdate", $this->MODULE_ID, '\Ddi\Mautic\EventHandlers\OnAfterCrmLeadUpdateHandler', "handler");

}

	function InstallFiles($arParams = array()){

	    //

        $rsData = CUserTypeEntity::GetList( array(), array('ENTITY_ID'=>'CRM_LEAD','FIELD_NAME'=>'UF_MAUTIC_ID') );
        if($arRes = $rsData->Fetch())
        {

        } else {
            $arFields = Array(
                "ENTITY_ID" => "CRM_LEAD",
                "FIELD_NAME" => 'UF_MAUTIC_ID',
                "USER_TYPE_ID" => "integer",
                "EDIT_FORM_LABEL" => Array("en"=>'UF_MAUTIC_ID')
            );
            $obUserField  = new CUserTypeEntity;
            $obUserField->Add($arFields);
        }


		$path = $this->GetPath()."/install/components";

		if (\Bitrix\Main\IO\Directory::isDirectoryExists($path)){
			CopyDirFiles($path, $_SERVER["DOCUMENT_ROOT"]."/bitrix/components", true, true);
		}

		if (\Bitrix\Main\IO\Directory::isDirectoryExists($path = $this->GetPath().'/admin')){
			CopyDirFiles($this->GetPath()."/install/admin/", $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin");
			if ($dir = opendir($path)){
				while (false !== $item = readdir($dir)){
					if (in_array($item, $this->exclusionAdminFiles))
						continue;
					file_put_contents($_SERVER['DOCUMENT_ROOT'].'/bitrix/admin/'.$item,
						'<'.'? require($_SERVER["DOCUMENT_ROOT"]."'.$this->GetPath(true).'/admin/'.$item.'");?'.'>');
				}
				closedir($dir);
			}
		}

		if (\Bitrix\Main\IO\Directory::isDirectoryExists($path = $this->GetPath().'/install/files')){
			$this->copyArbitraryFiles();
		}

		return true;
	}

	function UnInstallFiles(){
		\Bitrix\Main\IO\Directory::deleteDirectory($_SERVER["DOCUMENT_ROOT"].'/bitrix/components/'.$this->MODULE_ID.'/');

		if (\Bitrix\Main\IO\Directory::isDirectoryExists($path = $this->GetPath().'/admin')){
			DeleteDirFiles($_SERVER["DOCUMENT_ROOT"].$this->GetPath().'/install/admin/', $_SERVER["DOCUMENT_ROOT"].'/bitrix/admin');
			if ($dir = opendir($path)){
				while (false !== $item = readdir($dir)){
					if (in_array($item, $this->exclusionAdminFiles))
						continue;
					\Bitrix\Main\IO\File::deleteFile($_SERVER['DOCUMENT_ROOT'].'/bitrix/admin/'.$this->MODULE_ID.'_'.$item);
				}
				closedir($dir);
			}
		}

		if (\Bitrix\Main\IO\Directory::isDirectoryExists($path = $this->GetPath().'/install/files')){
			$this->deleteArbitraryFiles();
		}

		return true;
	}

	function copyArbitraryFiles(){
		$rootPath = $_SERVER["DOCUMENT_ROOT"];
		$localPath = $this->GetPath().'/install/files';

		$dirIterator = new RecursiveDirectoryIterator($localPath, RecursiveDirectoryIterator::SKIP_DOTS);
		$iterator = new RecursiveIteratorIterator($dirIterator, RecursiveIteratorIterator::SELF_FIRST);

		foreach ($iterator as $object){
			$destPath = $rootPath.DIRECTORY_SEPARATOR.$iterator->getSubPathName();
			($object->isDir()) ? mkdir($destPath) : copy($object, $destPath);
		}
	}

	function deleteArbitraryFiles(){
		$rootPath = $_SERVER["DOCUMENT_ROOT"];
		$localPath = $this->GetPath().'/install/files';

		$dirIterator = new RecursiveDirectoryIterator($localPath, RecursiveDirectoryIterator::SKIP_DOTS);
		$iterator = new RecursiveIteratorIterator($dirIterator, RecursiveIteratorIterator::SELF_FIRST);

		foreach ($iterator as $object){
			if (!$object->isDir()){
				$file = str_replace($localPath, $rootPath, $object->getPathName());
				\Bitrix\Main\IO\File::deleteFile($file);
			}
		}
	}

	function createNecessaryIblocks(){
		return true;
	}

	function deleteNecessaryIblocks(){
		return true;
	}

	function createNecessaryUserFields(){
		return true;
	}

	function deleteNecessaryUserFields(){
		return true;
	}

	function createNecessaryMailEvents(){
		return true;
	}

	function deleteNecessaryMailEvents(){
		return true;
	}

	function isVersionD7(){
		return CheckVersion(\Bitrix\Main\ModuleManager::getVersion('main'), '14.00.00');
	}

	function GetPath($notDocumentRoot = false){
		if ($notDocumentRoot){
			return str_ireplace(Application::getDocumentRoot(), '', dirname(__DIR__));
		}else{
			return dirname(__DIR__);
		}
	}

	function getSitesIdsArray(){
		$ids = Array();
		$rsSites = CSite::GetList($by = "sort", $order = "desc");
		while ($arSite = $rsSites->Fetch()){
			$ids[] = $arSite["LID"];
		}

		return $ids;
	}

	function DoInstall(){

		global $APPLICATION;
		if ($this->isVersionD7()){
			\Bitrix\Main\ModuleManager::registerModule($this->MODULE_ID);

			$this->InstallDB();
			$this->createNecessaryMailEvents();
			$this->InstallEvents();
			$this->InstallFiles();
			$this->SetconfigTemplate();
		}else{
			$APPLICATION->ThrowException(Loc::getMessage("DDI_MAUTIC_INSTALL_ERROR_VERSION"));
		}

		$APPLICATION->IncludeAdminFile(Loc::getMessage("DDI_MAUTIC_INSTALL"), $this->GetPath()."/install/step.php");
	}

	function DoUninstall(){

		global $APPLICATION;

		$context = Application::getInstance()->getContext();
		$request = $context->getRequest();

		$this->UnInstallFiles();
		$this->deleteNecessaryMailEvents();
		$this->UnInstallEvents();

		if ($request["savedata"] != "Y")
			$this->UnInstallDB();

		\Bitrix\Main\ModuleManager::unRegisterModule($this->MODULE_ID);

		$APPLICATION->IncludeAdminFile(Loc::getMessage("DDI_MAUTIC_UNINSTALL"), $this->GetPath()."/install/unstep.php");
	}


	function SetconfigTemplate(){
        $rsTemplates = CSite::GetTemplateList("s1");
        while($arTemplate = $rsTemplates->Fetch()) $result[]  = $arTemplate;
        $ssort = array();
        $res2 = array();
        $templ = true;

        foreach($result as $template){
            unset($template["ID"]);
            //unset($template["SITE_ID"]);
            array_push($ssort, intval($template["SORT"]));
            $res2[]=$template;

            if($template["CONDITION"]==="CSite::InDir('/mautic.php')"){
                $templ = false;
            }
        }

        if($templ){
            $res2[] = array(
                'SITE_ID' => 's1',
                'CONDITION' => "CSite::InDir('/mautic.php')",
                'SORT' => max($ssort)+100,
                'TEMPLATE' => "pub"
            );

            $obSite = new CSite();
            $t = $obSite->Update("s1", array(
                'TEMPLATE'=>$res2
            ));
        }

    }
}
?>