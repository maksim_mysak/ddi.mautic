<?
namespace Ddi\Mautic\EventHandlers;

use Bitrix\Main\Localization;
Localization\Loc::loadMessages(__FILE__);

class OnAfterCrmLeadUpdateHandler{
	static public function handler(&$arFields){
        if (\Bitrix\Main\Loader::includeModule('ddi.mautic'))
        {
            if (\COption::GetOptionString("main","mautic_stop_event")!='Y') {
                \MauticBitrix::UpdateLeadMauticFromBitrix($arFields['ID']);
            }
        }

        return $arFields;
	}

}