<?
namespace Ddi\Mautic\EventHandlers;

use Bitrix\Main\Localization;
Localization\Loc::loadMessages(__FILE__);
use Bitrix\Main\Diag\Debug;

class OnAfterCrmLeadAddHandler{
    static public function handler(&$arFields){
        if (\Bitrix\Main\Loader::includeModule('ddi.mautic'))
        {
            if (\COption::GetOptionString("main","mautic_stop_event")!='Y') {
                \MauticBitrix::AddLeadMauticFromBitrix($arFields['ID'], $arFields);
            }
        }

        return $arFields;
    }

}