
## Configuration mautic site
 1. delete cache in mautic web-server
    `php app/console cache:clear`
    
    If this command throws a PHP error, you can try to nuke the cache folder like this:
    `rm -rf app/cache`
    
 2.Go to  http://mautic.site/s/config/edit
    
    API Settings->Enable HTTP basic auth?->Yes
    
    example - http://i.prntscr.com/Jpz3THFRRE6zlu608xhYjQ.png

 3. Create api user 

   http://mautic.site/s/users/new
   User role - Administrator
   
 4. Create Webhooks 

   Go to http://site.com/s/webhooks/1
   Create new webhook 
   http://mautic.site/s/webhooks/new
   
   Name - Crm bitrix24
   
   Webhook Events  - 'Contact Updated Event' and 'Contact Identified Event'
   
   Webhook POST Url - http://bx24.site/mautic.php 
     
   example - http://i.prntscr.com/DK_2aqujRFym9VHZXU_b7Q.png
   
## Configuration bitrix24 site

1. You need to download the module and install it on the site

 Download repository: https://bitbucket.org/maksim_mysak/ddi.mautic/downloads/ddi.mautic.tar.gz
    
  example install modules bitrix24 - https://youtu.be/EtlPJ1tYbrA
 
 
 ##Demo 
 
 demonstrations example - https://youtu.be/kfpwjJ-eRXo